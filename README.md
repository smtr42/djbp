DjBP
====

or _my [Django][django] Boiler Plate_

What?
-----

A compilation of my personal (and mostly perfectible) _Django-starting-practices_ for the record.

With ?
------

* nice [pytest-dash-something][req-dev] stuff
* geat work of _Gitlab_ [pages & CI][glci]
* _"badarch"_ [KNACSS][knacss] HTML/CSS template
* _bug catching_ client-side [`git hooks`][githooks]
* [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)][black]

Where?
------

This repo will not merge branches in `master`, and keep each _Django app_ in a dedicated `branch` to ease _bootstrap_ :

### [`master`][master] :

Keep the speech

### [`core`][core] [![pipeline status](http://gitlab.com/free_zed/djbp/badges/core/pipeline.svg)](http://gitlab.com/free_zed/djbp/commits/core) :

The _project_ (_Django_ meaning) with tests, CI, requirement, base-template, …

### [`one-to-one`][onetoone] [![pipeline status](http://gitlab.com/free_zed/djbp/badges/one-to-one/pipeline.svg)](http://gitlab.com/free_zed/djbp/commits/one-to-one) :

A _CRUD app_ containing two tables over a `OneToOne` relationship built to explore _Djangos_ `WhateverFormClasses`.

### [`filu`][filu] [![pipeline status](http://gitlab.com/free_zed/djbp/badges/filu/pipeline.svg)](http://gitlab.com/free_zed/djbp/commits/filu) :

A basic _file upload_ application

### [`dependent-dropdown`][depdrop] [![pipeline status](http://gitlab.com/free_zed/djbp/badges/dependent-dropdown/pipeline.svg)](http://gitlab.com/free_zed/djbp/commits/dependent-dropdown) :

A dependent drop-down lists in form (based on work of _[Vitor Freitas](https://simpleisbetterthancomplex.com/tutorial/2018/01/29/how-to-implement-dependent-or-chained-dropdown-list-with-django.html).

Who?
----

[Everybody](/LICENSE.md) ! Feel free to [fork][fork] or [contribute][issue].


[black]:    https://github.com/psf/black "The uncompromising code formatter"
[core]:     https://gitlab.com/free_zed/djbp/tree/core/
[django]:   https://djangoproject.com ""
[filu]:     https://gitlab.com/free_zed/djbp/tree/filu/
[fork]:     https://gitlab.com/free_zed/djbp/-/forks/new
[githooks]: https://gitlab.com/free_zed/djbp/tree/master/docs/git-hooks
[glci]:     https://gitlab.com/free_zed/djbp/tree/core/.gitlab-ci.yml
[issue]:    https://gitlab.com/free_zed/djbp/-/issues/new
[knacss]:   https://schnaps.it/ "Générateur de template HTML5 since 1664"
[master]:   https://gitlab.com/free_zed/djbp/tree/master/
[onetoone]: https://gitlab.com/free_zed/djbp/tree/one-to-one/
[req-dev]:  https://gitlab.com/free_zed/djbp/tree/core/requirements-dev.txt
[depdrop]:  https://gitlab.com/free_zed/djbp/tree/dependent-dropdown/
